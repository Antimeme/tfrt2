import { defineStore } from 'pinia'
import { useInputStore } from './input';
import { useMemberStore } from './member';

export const useFactionStore = defineStore('faction', {
    state: () => {
        let factions = {};
		let teams = {};
        
        
        try {
            if (localStorage.allFactions) {
                factions =  JSON.parse(localStorage.allFactions);
				const members = useMemberStore();
				for (const faction of Object.values(factions)) {
					for (const member_id in faction.members) {
						faction.members[member_id].id = member_id;
						members.updateMember({
							...faction.members[member_id],
							faction_id: faction.id,
							last_sync: Date.now(),
						});
					}

				}
            }
        } catch (error) {
			console.log("ERROR LOADING FACTIONS", error);
		}
      
		try {
            if (localStorage.allTeams) {
                teams =  JSON.parse(localStorage.allTeams);
            }
        } catch {}

        return {
            factions,
			teams,
        }
    },
    
    getters: {
        current_faction(state) {
            const input = useInputStore();
            const faction_id = this.selected_faction_id || input.faction_id;
            return state.factions[faction_id];
        },
        current_team(state) {
            const input = useInputStore();
            const team_name = this.selected_team_name;
			return team_name ? state.teams[team_name] : null;
        },
    },

    actions: {
        addFaction(faction) {
            this.factions[faction.ID] = {...faction, id: faction.ID};
            localStorage.allFactions = JSON.stringify(this.factions);
        },
		
        removeFaction(faction_id) {
			delete this.factions[faction_id];
			localStorage.allFactions = JSON.stringify(this.factions);
        },

        selectFaction(faction_id) {
            this.selected_faction_id = faction_id;
            localStorage.selectedFaction = faction_id;
        },

        addTeam(team) {
            this.teams[team.name] = team;
            localStorage.allTeams = JSON.stringify(this.teams);
        },
		
        removeTeam(team_name) {
			delete this.teams[team_name];
			localStorage.allTeams = JSON.stringify(this.teams);
        },

        selectTeam(team_name) {
            this.selected_team_name = team_name;
            localStorage.selectedTeam = team_name;
        }

    }
})