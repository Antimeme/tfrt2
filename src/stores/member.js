import { defineStore } from 'pinia'
import {  HospStatusSettings, OnlineStatusSettings, LocationStatusSettings, useFiltersStore } from './filters';
import { useInputStore } from './input';
import { useFactionStore } from './faction';
import { toRaw } from 'vue';
export const useMemberStore = defineStore('member', {
	state: () => {
		return {
			faction_members: {
			},
			team_members: {

			}
		}
	},

	actions: {
		updateMember(member) {
			const faction_id = member.faction_id || member.faction?.faction_id;

			if (!this.faction_members[faction_id]) {
				this.faction_members[parseInt(faction_id, 10)] = {};
			}


			const original_member = this.faction_members[faction_id][member.id];
			const new_member = {
				...original_member,
				...member
			};

			if (member.last_action) {
				new_member.online_type = member.last_action.status === 'Online' ? OnlineStatusSettings.ONLINE : (member.last_action.status === 'Offline' ? OnlineStatusSettings.OFFLINE : OnlineStatusSettings.IDLE)
			}
			
			if (member.basicicons) {
				new_member.location_type = member.basicicons.icon71 === 'Traveling' ?  LocationStatusSettings.TRAVELLING : LocationStatusSettings.IN_TORN;
			}

			if (member?.status?.state) {
				new_member.hosp_type = member.status.state === 'Hospital' ? 
					(member.status.details.includes('Hospitalized by') ? 
						HospStatusSettings.HOSPITALIZED : HospStatusSettings.IN_HOSPITAL
					): HospStatusSettings.HEALTHY;
			}
			
			this.faction_members[faction_id][member.id] = new_member;
		},

		updateTeamMember(member, team_name) {
			

			if (!this.team_members[team_name]) {
				this.team_members[team_name] = {};
			}


			const original_member = this.team_members[team_name][member.id];
			const new_member = {
				...original_member,
				...member
			};

			if (member.last_action) {
				new_member.online_type = member.last_action.status === 'Online' ? OnlineStatusSettings.ONLINE : (member.last_action.status === 'Offline' ? OnlineStatusSettings.OFFLINE : OnlineStatusSettings.IDLE)
			}
			
			if (member.basicicons) {
				new_member.location_type = member.basicicons.icon71 === 'Traveling' ?  LocationStatusSettings.TRAVELLING : LocationStatusSettings.IN_TORN;
			}

			if (member?.status?.state) {
				new_member.hosp_type = member.status.state === 'Hospital' ? 
					(member.status.details.includes('Hospitalized by') ? 
						HospStatusSettings.HOSPITALIZED : HospStatusSettings.IN_HOSPITAL
					): HospStatusSettings.HEALTHY;
			}
			
			this.team_members[team_name][member.id] = new_member;
		},

		togglePinned(member, team_name) {
			if (team_name) this.updateTeamMember({
				...member,
				pinned: member.pinned === true  ? false : true
			}, team_name);
			else this.updateMember({
				...member,
				pinned: member.pinned === true  ? false : true
			});
		}
	}
})