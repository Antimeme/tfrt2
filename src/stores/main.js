import { defineStore } from 'pinia';
import axios from 'axios';

import { useMemberStore } from "./member";
import { useFactionStore } from "./faction";
import { useApiStore } from "./api";
import { useSettingsStore } from "./settings";
import { useFiltersStore } from "./filters";
import { useInputStore } from "./input";
import { useRouteStore } from './route';
import { sleep } from '../util';

export const useMainStore = defineStore('store', {
	state: () => ({
		member: useMemberStore(),
		faction: useFactionStore(),
		api: useApiStore(),
		settings: useSettingsStore(),
		filters: useFiltersStore(),
		input: useInputStore(),
		route: useRouteStore(),
		now: Date.now(),
	}),

	actions: {
		setNow(now) {
			this.now = now;
		},

		async apiRequest(endpoint) {
			let cur_unix = Date.now();
			if (this.api.quota_remaining <= this.settings.min_quota) {
				const wait_time = cur_unix - this.api.throttling.requests[0];
				await sleep(Math.max(this.settings.min_delay, wait_time));
			}
			this.api.setApiRequestCount(this.api.throttling.api_request_count + 1);
			cur_unix = Date.now();
			this.api.throttling.requests.push(cur_unix + 60_000);

			
			try {
				const response = await axios.get(endpoint);
				setTimeout(() => {
					this.api.throttling.api_request_count -= 1;
					this.api.throttling.requests.shift();
				}, 60_000);
				await sleep(this.settings.min_delay)
				if (response.status != 200) {
					throw Error("Error: Invalid HTTP status:", response.status)
				} 
				else if (response.data.error) {
					throw Error(`Error From API: ${response.data.error.error}`);
				}
				return response;
			} catch (error) {
				console.error(`${endpoint}`, error);;
				return null;
			}
		},
		
		
		// Fetching
		async fetchSingleFaction(faction_id) {
			const id = parseInt(faction_id, 10);
			console.log("FETCH SINGLE FACTION", id, faction_id);
			if (Number.isInteger(id)) {
				const response = await this.apiRequest(
					`https://api.torn.com/faction/${id}?key=${this.settings.api_key}`
				);
				
				if (response && !response.data?.error) {
					const faction = response.data;
					for (const member_id in response.data.members) {
						response.data.members[member_id].id = member_id;
						this.member.updateMember({
							...response.data.members[member_id],
							faction_id: parseInt(id, 10),
							last_sync: Date.now(),
						});
					}
					this.faction.addFaction(faction);
				}
			}
		},

		async fetchSingleMember(member_id, team_name = false ) {

			const response = await this.apiRequest(
				`https://api.torn.com/user/${member_id}?key=${this.settings.api_key}`
			);

			if (response && !response.data?.error) {
				const date = Date.now();
				const new_member = response.data;
				new_member.id = member_id;
				new_member.last_sync = date;
				new_member.last_full_sync = date;
				

				if (new_member.faction) {
					new_member.faction_id = new_member.faction.id;
				}

				if (team_name) this.member.updateTeamMember(new_member, team_name);
				else this.member.updateMember(new_member);
			}
		},
	}
});