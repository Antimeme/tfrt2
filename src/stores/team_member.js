import { defineStore } from 'pinia'
import {  HospStatusSettings, OnlineStatusSettings, LocationStatusSettings, useFiltersStore } from './filters';
import { useInputStore } from './input';
import { useFactionStore } from './faction';
import { toRaw } from 'vue';
export const useTeamMemberStore = defineStore('teamMember', {
    state: () => {
        return {
			team_members: {
			},
        }
    },
    
    getters: {
		current_team_members(state) {
            const factions = useFactionStore();
            const team_name = factions.selected_team_name;
            if (state.team_members[team_name]) return Object.values(state.team_members[team_name]);
            else return [];
        },

        pinned_members(state) {
            return Object.values(state.faction_members).filter(member => member.pinned === true);
        },
    },

    actions: {
        updateMember(member, team_name) {
            if (!this.team_members[team_name]) {
                this.team_members[team_name] = {};
            }


            const original_member = this.team_members[team_name][member.id];
            const new_member = {
                ...original_member,
                ...member
            };

            if (member.last_action) {
                new_member.online_type = member.last_action.status === 'Online' ? OnlineStatusSettings.ONLINE : (member.last_action.status === 'Offline' ? OnlineStatusSettings.OFFLINE : OnlineStatusSettings.IDLE)
            }
            
            if (member.basicicons) {
                new_member.location_type = member.basicicons.icon71 === 'Traveling' ?  LocationStatusSettings.TRAVELLING : LocationStatusSettings.IN_TORN;
            }

            if (member?.status?.state) {
                new_member.hosp_type = member.status.state === 'Hospital' ? 
                    (member.status.details.includes('Hospitalized by') ? 
                        HospStatusSettings.HOSPITALIZED : HospStatusSettings.IN_HOSPITAL
                    ): HospStatusSettings.HEALTHY;
            }
            
            this.team_members[team_name][member.id] = new_member;
        },

        togglePinned(member, team_name) {
            this.updateMember({
                ...member,
                pinned: member.pinned === true  ? false : true
            }, team_name);
        }
    }
})