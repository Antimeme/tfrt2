import { defineStore } from 'pinia'
import { useInputStore } from './input';

export const useApiStore = defineStore('api', {
    state: () => ({
        throttling: {
            api_request_count: 0,
            requests: []
        },
        timers: {
            hosp_info_interval: null,
            last_sync: parseInt(localStorage.last_sync, 10) || 0,
        },
    }),
    
    getters: {
        quota_remaining(state) {
            return 100 - state.throttling.api_request_count;
        },
    },

    actions: {
        setApiRequestCount(count) {
            this.throttling.api_request_count = count;
        },

        setLastSync(value) {
            this.timers.last_sync = value;
            localStorage.last_sync = value;
        }
    }
});