import { defineStore } from 'pinia'


const API_KEY_DISCLAIMER = "TORN API KEY, DO NOT SEND TO ANYONE:";
export const useSettingsStore = defineStore('settings', {
    state: () => ({
        api_key: localStorage.TAK?.replace(API_KEY_DISCLAIMER, '')|| null,
        min_quota: parseInt(localStorage.min_quota_setting, 10) || 25,
        min_delay: parseInt(localStorage.min_delay_setting, 10) || 100,
        autorefresh: {
            enabled: false,
            interval: parseInt(localStorage.autorefresh_interval_setting, 10)|| 240,
            timer: null,
        },
    }),
    
    getters: {
        
    },

    actions: {
        setApiKey(value) {
            this.api_key = value;
            localStorage.TAK = `${API_KEY_DISCLAIMER}${value}`;
        },

        setAutorefreshEnabled(value) {
            this.autorefresh.enabled = value;
        }
    }
})