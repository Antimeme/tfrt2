import { defineStore } from 'pinia'
import { useFactionStore } from './faction'
import { useRouteStore } from './route'

export const HospStatusSettings = {
	ALL: 'ALL',
	HOSPITALIZED: 'HOSPITALIZED',
	IN_HOSPITAL: 'IN_HOSPITAL',
	HEALTHY: 'HEALTHY'
}

export const OnlineStatusSettings = {
	ALL: 'ALL',
	ONLINE: 'ONLINE',
	ONLINE_IDLE: 'ONLINE_IDLE',
	IDLE: 'IDLE',
	OFFLINE: 'OFFLINE',
}

export const LocationStatusSettings = {
	ALL: 'ALL',
	IN_TORN: 'IN_TORN',
    TRAVELLING: 'TRAVELLING'
}

const default_filters = {
    online: OnlineStatusSettings.ALL,
    level: 0,
    hospitalized: HospStatusSettings.ALL,
    location: LocationStatusSettings.ALL,
    revivable: false,
}

export const useFiltersStore = defineStore('filters', {
    state: () => ({
        by_faction: {},
        online: localStorage.online_setting ?  JSON.parse(localStorage.online_setting) : OnlineStatusSettings.ALL,
        level: localStorage.level_setting ?  JSON.parse(localStorage.level_setting) : 0,
        hospitalized: localStorage.hospitalized_setting ?  JSON.parse(localStorage.hospitalized_setting) : HospStatusSettings.ALL,
        location: localStorage.location_setting ?  JSON.parse(localStorage.location_setting) : LocationStatusSettings.ALL,
        revivable: localStorage.revivable_setting ?  JSON.parse(localStorage.revivable_setting) : false,
    }),
    
    getters: {
        current_filters() {
            const routerStore = useRouteStore();
            const faction_id = routerStore.state.params.faction_id || routerStore.state.params.team_name;
            if (!faction_id) return {...default_filters};

            if (!this.by_faction[faction_id]) {
                let filters = {...default_filters};
                const from_backup = localStorage[`${faction_id}_filters`];
                
                try {
                    if (from_backup) {
                        this.by_faction[faction_id] = JSON.parse(from_backup);
                        return this.by_faction[faction_id];
                    }
                } catch {};

                return filters;
            } else { 
                return this.by_faction[faction_id];
            }
        }
    },

    actions: {
        setFilter(filter, value) {
			const routerStore = useRouteStore();
			const faction_id = routerStore.state.params.faction_id || routerStore.state.params.team_name ;

			if (!this.by_faction[faction_id]) {
                this.by_faction[faction_id] = {...default_filters};
            };

            this.by_faction[faction_id][filter] = value;
            localStorage[`${faction_id}_filters`] = JSON.stringify(this.by_faction[faction_id]);
        },

    }
})