import { defineStore } from 'pinia'


export const useRouteStore = defineStore('route', {
	state: () => ({
		state: {
			fullPath: "/",
			path: "/",
			query: {},
			hash: "",
			name: "",
			params: {
				
			},
			matched: [],
			meta: {},
			redirectedFrom: null,
			href: "/"
		}
	}),

	actions: {
		updateState(state) {
			this.state = state;
		}
	},
});