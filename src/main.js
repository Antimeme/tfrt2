import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import { useMainStore } from './stores/main';

import './assets/main.scss';


// Apple decided that the viewport height unit should not match up to the viewport height on mobile devices.
// They had shitty reasons AND implemented the wrong solution.
function fuckApple() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh-1', `${vh}px`);
}
window.addEventListener('resize', fuckApple);
fuckApple();
const app = createApp(App)

app.use(createPinia());
app.config.globalProperties.store = useMainStore();
app.use(router);

app.mount('#app');
