import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '../views/DashboardView.vue'
import FactionView from '../views/FactionView.vue'
import TeamView from '../views/TeamView.vue'
import AboutView from '../views/AboutView.vue'
import { useRouteStore } from '../stores/route';

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
    {
		path: '/',
		name: 'home',
		component: AboutView
    },
    {
      path: '/team/:team_name',
      name: 'View Team',
      component: TeamView
    },
    {
		path: '/faction/:faction_id',
		name: 'View Faction',
		component: FactionView
    },
    {
		path: '/about',
		name: 'About',
		// route level code-splitting
		// this generates a separate chunk (About.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import('../views/AboutView.vue')
    }
],
})
router.beforeEach(
	(to, from) => {
		const routeStore = useRouteStore();
	  console.log("ROUTE", to, from);
	  routeStore.updateState(to);
	  
	}
)
export default router
